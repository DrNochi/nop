﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace nOP
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Token> tokenizedSource;

            using (StreamReader sourceStream = new StreamReader(args[0]))
            {
                tokenizedSource = Lex(sourceStream, args[0]);
            }

            AbstractSyntaxTree ast = Parse(tokenizedSource, args[0]);

            Execute(ast, args[0]);
        }

        static List<Token> Lex(StreamReader sourceStream, string fileName)
        {
            List<Token> tokenizedSource = new List<Token>();

            int currentLineNumber = 1;
            int currentLineColumn = 0;
            StringBuilder currentLine = new StringBuilder();

            Token currentToken = new Token();
            StringBuilder currentLexeme = new StringBuilder();

            int nextCharUTF32;
            while ((nextCharUTF32 = sourceStream.Read()) != -1)
            {
                string nextChar = char.ConvertFromUtf32(nextCharUTF32);

                if (nextChar == "\n")
                {
                    ++currentLineNumber;
                    currentLineColumn = 0;
                    currentLine.Clear();
                }
                else
                {
                    ++currentLineColumn;
                    currentLine.Append(nextChar);
                }

                switch (currentToken.Type)
                {
                    case TokenType.IdentifierLiteral:
                        if (char.IsLetterOrDigit(nextChar, 0))
                        {
                            currentLexeme.Append(nextChar);

                            switch (currentLexeme.ToString())
                            {
                                // Keywords
                                case "function":
                                    currentToken.Type = TokenType.FunctionKeyword;
                                    break;

                                // Instructions
                                case "call":
                                    currentToken.Type = TokenType.CallInstruction;
                                    break;

                                case "not":
                                    currentToken.Type = TokenType.NotInstruction;
                                    break;

                                case "and":
                                    currentToken.Type = TokenType.AndInstruction;
                                    break;

                                case "or":
                                    currentToken.Type = TokenType.OrInstruction;
                                    break;

                                case "xor":
                                    currentToken.Type = TokenType.XorInstruction;
                                    break;

                                case "add":
                                    currentToken.Type = TokenType.AddInstruction;
                                    break;

                                case "sub":
                                    currentToken.Type = TokenType.SubInstruction;
                                    break;

                                case "mul":
                                    currentToken.Type = TokenType.MulInstruction;
                                    break;

                                case "div":
                                    currentToken.Type = TokenType.DivInstruction;
                                    break;

                                case "rem":
                                    currentToken.Type = TokenType.RemInstruction;
                                    break;

                                case "PRINT":
                                    currentToken.Type = TokenType.DEBUGPRINTINSTRUCTION;
                                    break;
                            }

                            continue;
                        }
                        break;

                    case TokenType.IntegerLiteral:
                        if (char.IsDigit(nextChar, 0))
                        {
                            currentLexeme.Append(nextChar);
                            continue;
                        }
                        break;
                }

                if (char.IsWhiteSpace(nextChar, 0))
                {
                    if (currentLexeme.Length > 0)
                    {
                        if (currentToken.Type == TokenType.Unknown)
                        {
                            LexerError("Unexpected whitespace", fileName, currentLine.ToString(), currentLineNumber, currentLineColumn);
                        }
                        else
                        {
                            currentToken.Lexeme = currentLexeme.ToString();
                            tokenizedSource.Add(currentToken);
                        }

                        currentToken = new Token();
                        currentLexeme = currentLexeme.Clear();
                    }

                    continue;
                }

                switch (nextChar)
                {
                    case "{":
                        if (currentLexeme.Length > 0)
                        {
                            if (currentToken.Type == TokenType.Unknown)
                            {
                                LexerError("Unexpected brace", fileName, currentLine.ToString(), currentLineNumber, currentLineColumn);
                            }
                            else
                            {
                                currentToken.Lexeme = currentLexeme.ToString();
                                tokenizedSource.Add(currentToken);
                            }

                            currentToken = new Token();
                            currentLexeme = currentLexeme.Clear();
                        }

                        currentToken.Type = TokenType.OpeningBrace;
                        break;

                    case "}":
                        if (currentLexeme.Length > 0)
                        {
                            if (currentToken.Type == TokenType.Unknown)
                            {
                                LexerError("Unexpected brace", fileName, currentLine.ToString(), currentLineNumber, currentLineColumn);
                            }
                            else
                            {
                                currentToken.Lexeme = currentLexeme.ToString();
                                tokenizedSource.Add(currentToken);
                            }

                            currentToken = new Token();
                            currentLexeme = currentLexeme.Clear();
                        }

                        currentToken.Type = TokenType.ClosingBrace;
                        break;

                    case ",":
                        if (currentLexeme.Length > 0)
                        {
                            if (currentToken.Type == TokenType.Unknown)
                            {
                                LexerError("Unexpected comma", fileName, currentLine.ToString(), currentLineNumber, currentLineColumn);
                            }
                            else
                            {
                                currentToken.Lexeme = currentLexeme.ToString();
                                tokenizedSource.Add(currentToken);
                            }

                            currentToken = new Token();
                            currentLexeme = currentLexeme.Clear();
                        }

                        currentToken.Type = TokenType.CommaSeparator;
                        break;

                    case ";":
                        if (currentLexeme.Length > 0)
                        {
                            if (currentToken.Type == TokenType.Unknown)
                            {
                                LexerError("Unexpected semicolon", fileName, currentLine.ToString(), currentLineNumber, currentLineColumn);
                            }
                            else
                            {
                                currentToken.Lexeme = currentLexeme.ToString();
                                tokenizedSource.Add(currentToken);
                            }

                            currentToken = new Token();
                            currentLexeme = currentLexeme.Clear();
                        }

                        currentToken.Type = TokenType.SemicolonSeparator;
                        break;

                    case "=":
                        if (currentLexeme.Length > 0)
                        {
                            if (currentToken.Type == TokenType.Unknown)
                            {
                                LexerError("Unexpected character", fileName, currentLine.ToString(), currentLineNumber, currentLineColumn);
                            }
                            else
                            {
                                currentToken.Lexeme = currentLexeme.ToString();
                                tokenizedSource.Add(currentToken);
                            }

                            currentToken = new Token();
                            currentLexeme = currentLexeme.Clear();
                        }

                        currentToken.Type = TokenType.AssignmentOperator;
                        break;

                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                    case "6":
                    case "7":
                    case "8":
                    case "9":
                    case "0":
                    case "-":
                        if (currentLexeme.Length > 0)
                        {
                            if (currentToken.Type == TokenType.Unknown)
                            {
                                LexerError("Unexpected character", fileName, currentLine.ToString(), currentLineNumber, currentLineColumn);
                            }
                            else
                            {
                                currentToken.Lexeme = currentLexeme.ToString();
                                tokenizedSource.Add(currentToken);
                            }

                            currentToken = new Token();
                            currentLexeme = currentLexeme.Clear();
                        }

                        currentToken.Type = TokenType.IntegerLiteral;
                        break;

                    case "$":
                        if (currentLexeme.Length > 0)
                        {
                            if (currentToken.Type == TokenType.Unknown)
                            {
                                LexerError("Unexpected character", fileName, currentLine.ToString(), currentLineNumber, currentLineColumn);
                            }
                            else
                            {
                                currentToken.Lexeme = currentLexeme.ToString();
                                tokenizedSource.Add(currentToken);
                            }

                            currentToken = new Token();
                            currentLexeme = currentLexeme.Clear();
                        }

                        currentToken.Type = TokenType.LowLevelDirective;
                        break;

                    default:
                        currentToken.Type = TokenType.IdentifierLiteral;
                        break;
                }

                currentLexeme.Append(nextChar);
            }

            if (currentLexeme.Length > 0)
            {
                if (currentToken.Type == TokenType.Unknown)
                {
                    LexerError("Unexpected end of file", fileName, currentLine.ToString(), currentLineNumber, currentLineColumn);
                }
                else
                {
                    currentToken.Lexeme = currentLexeme.ToString();
                    tokenizedSource.Add(currentToken);
                }

                currentToken = new Token();
                currentLexeme = currentLexeme.Clear();
            }

            return tokenizedSource;
        }

        static void LexerError(string errorMessage, string currentFile, string currentLine, int currentLineNumber, int currentLineColumn)
        {
            Console.Error.WriteLine("\n");

            string lineNumber = currentLineNumber.ToString();

            Console.Error.WriteLine("[ERROR] " + currentFile + ":" + lineNumber + " (In column " + currentLineColumn + ") - Lexing failed!");
            Console.Error.WriteLine(lineNumber + " | " + currentLine);

            int offset = lineNumber.Length + 3 + currentLineColumn;

            if (offset - 3 < errorMessage.Length)
            {
                Console.Error.WriteLine("^-- ".PadLeft(offset + 3) + errorMessage);
            }
            else
            {
                Console.Error.WriteLine(errorMessage.PadLeft(offset - 3) + " --^");
            }

            Console.Error.WriteLine("\n");
        }

        static AbstractSyntaxTree Parse(List<Token> tokenizedSource, string fileName)
        {
            AbstractSyntaxTree ast = new AbstractSyntaxTree();

            for (int i = 0; i < tokenizedSource.Count; ++i)
            {
                ast.Functions.Add(AbstractSyntaxTree.Function.Parse(tokenizedSource, ref i, fileName));
            }

            return ast;
        }

        static void Execute(AbstractSyntaxTree ast, string fileName)
        {
            foreach (AbstractSyntaxTree.Function function in ast.Functions)
            {
                if (function.Name == "main")
                {
                    function.Execute(new List<Dictionary<string, int>>(), ast, fileName);
                    break;
                }
            }
        }
    }

    class AbstractSyntaxTree
    {
        public List<Function> Functions { get; set; } = new List<Function>();

        public abstract class Statement
        {
            public class Block : Statement
            {
                public List<Statement> Statements { get; set; } = new List<Statement>();

                public static new Block Parse(List<Token> tokenizedSource, ref int sourceIndex, string fileName)
                {
                    Block block = new Block();

                    Token braceToken = tokenizedSource[sourceIndex];
                    if (braceToken.Type != TokenType.OpeningBrace)
                    {
                        ParserError("Opening brace expected", fileName);
                    }

                    for (++sourceIndex; sourceIndex < tokenizedSource.Count; ++sourceIndex)
                    {
                        braceToken = tokenizedSource[sourceIndex];
                        if (braceToken.Type == TokenType.ClosingBrace)
                        {
                            break;
                        }

                        block.Statements.Add(Statement.Parse(tokenizedSource, ref sourceIndex, fileName));
                    }

                    return block;
                }

                public override void Execute(List<Dictionary<string, int>> memory, AbstractSyntaxTree ast, string functionName, string fileName)
                {
                    memory.Add(new Dictionary<string, int>());

                    foreach (Statement statement in Statements)
                    {
                        statement.Execute(memory, ast, functionName, fileName);
                    }

                    memory.RemoveAt(memory.Count - 1);
                }
            }

            public abstract class LowLevel : Statement
            {
                public abstract class Expression : LowLevel
                {
                    public class Assignment : Expression
                    {
                        public Token Identifier { get; set; }
                        public Expression Value { get; set; }

                        public static new Assignment Parse(List<Token> tokenizedSource, ref int sourceIndex, string fileName)
                        {
                            Assignment assignment = new Assignment();

                            Token identifierToken = tokenizedSource[sourceIndex];
                            if (identifierToken.Type == TokenType.IdentifierLiteral)
                            {
                                assignment.Identifier = identifierToken;
                            }
                            else
                            {
                                ParserError("Identifier expected", fileName);
                            }

                            if (tokenizedSource[++sourceIndex].Type != TokenType.AssignmentOperator)
                            {
                                ParserError("Assignment operator expected", fileName);
                            }

                            Token valueToken = tokenizedSource[++sourceIndex];
                            switch (valueToken.Type)
                            {
                                case TokenType.IntegerLiteral:
                                    assignment.Value = DirectValue.Parse(tokenizedSource, ref sourceIndex, fileName);
                                    break;

                                case TokenType.CallInstruction:
                                case TokenType.NotInstruction:
                                case TokenType.AndInstruction:
                                case TokenType.OrInstruction:
                                case TokenType.XorInstruction:
                                case TokenType.AddInstruction:
                                case TokenType.SubInstruction:
                                case TokenType.MulInstruction:
                                case TokenType.DivInstruction:
                                case TokenType.RemInstruction:
                                case TokenType.DEBUGPRINTINSTRUCTION:
                                    assignment.Value = Instruction.Parse(tokenizedSource, ref sourceIndex, fileName);
                                    break;

                                default:
                                    ParserError("Value or instruction expected", fileName);
                                    break;
                            }

                            return assignment;
                        }

                        public override int ExecuteAndGetValue(List<Dictionary<string, int>> memory, AbstractSyntaxTree ast, string functionName, string fileName)
                        {
                            int value = Value.ExecuteAndGetValue(memory, ast, functionName, fileName);

                            foreach (Dictionary<string, int> frame in memory)
                            {
                                if (frame.ContainsKey(Identifier.Lexeme))
                                {
                                    ExecutionError("SSA variable already assigned (In function " + functionName + ")", fileName);
                                }
                            }

                            memory[memory.Count - 1].Add(Identifier.Lexeme, value);

                            return value;
                        }
                    }

                    public class DirectValue : Expression
                    {
                        public Token Value { get; set; }

                        public static new DirectValue Parse(List<Token> tokenizedSource, ref int sourceIndex, string fileName)
                        {
                            DirectValue value = new DirectValue();

                            Token valueToken = tokenizedSource[sourceIndex];
                            if (valueToken.Type == TokenType.IntegerLiteral)
                            {
                                value.Value = valueToken;
                            }
                            else
                            {
                                ParserError("Value expected", fileName);
                            }

                            return value;
                        }

                        public override int ExecuteAndGetValue(List<Dictionary<string, int>> memory, AbstractSyntaxTree ast, string functionName, string fileName)
                        {
                            return int.Parse(Value.Lexeme);
                        }
                    }

                    public class Instruction : Expression
                    {
                        public Token Operation { get; set; }
                        public List<Token> Operands { get; set; } = new List<Token>();

                        public static new Instruction Parse(List<Token> tokenizedSource, ref int sourceIndex, string fileName)
                        {
                            Instruction instruction = new Instruction();

                            Token operationToken = tokenizedSource[sourceIndex];
                            if (operationToken.Type == TokenType.CallInstruction
                                    || operationToken.Type == TokenType.NotInstruction
                                    || operationToken.Type == TokenType.AndInstruction
                                    || operationToken.Type == TokenType.OrInstruction
                                    || operationToken.Type == TokenType.XorInstruction
                                    || operationToken.Type == TokenType.AddInstruction
                                    || operationToken.Type == TokenType.SubInstruction
                                    || operationToken.Type == TokenType.MulInstruction
                                    || operationToken.Type == TokenType.DivInstruction
                                    || operationToken.Type == TokenType.RemInstruction
                                    || operationToken.Type == TokenType.DEBUGPRINTINSTRUCTION)
                            {
                                instruction.Operation = operationToken;
                            }
                            else
                            {
                                ParserError("Operation expected", fileName);
                            }

                            while (true)
                            {
                                Token operandToken = tokenizedSource[++sourceIndex];
                                if (operandToken.Type == TokenType.IdentifierLiteral
                                        || operandToken.Type == TokenType.IntegerLiteral)
                                {
                                    instruction.Operands.Add(operandToken);
                                }
                                else
                                {
                                    --sourceIndex;
                                    break;
                                }

                                Token seperatorToken = tokenizedSource[++sourceIndex];
                                if (seperatorToken.Type != TokenType.CommaSeparator)
                                {
                                    --sourceIndex;
                                    break;
                                }
                            }

                            return instruction;
                        }

                        public override int ExecuteAndGetValue(List<Dictionary<string, int>> memory, AbstractSyntaxTree ast, string functionName, string fileName)
                        {
                            switch (Operation.Type)
                            {
                                case TokenType.CallInstruction:
                                    foreach (AbstractSyntaxTree.Function function in ast.Functions)
                                    {
                                        if (function.Name == Operands[0].Lexeme)
                                        {
                                            function.Execute(new List<Dictionary<string, int>>(), ast, fileName);
                                            break;
                                        }
                                    }
                                    return 0;

                                case TokenType.NotInstruction:
                                    return ~GetOperandValue(Operands[0], memory, functionName, fileName);

                                case TokenType.AndInstruction:
                                    return GetOperandValue(Operands[0], memory, functionName, fileName) & GetOperandValue(Operands[1], memory, functionName, fileName);

                                case TokenType.OrInstruction:
                                    return GetOperandValue(Operands[0], memory, functionName, fileName) | GetOperandValue(Operands[1], memory, functionName, fileName);

                                case TokenType.XorInstruction:
                                    return GetOperandValue(Operands[0], memory, functionName, fileName) ^ GetOperandValue(Operands[1], memory, functionName, fileName);

                                case TokenType.AddInstruction:
                                    return GetOperandValue(Operands[0], memory, functionName, fileName) + GetOperandValue(Operands[1], memory, functionName, fileName);

                                case TokenType.SubInstruction:
                                    return GetOperandValue(Operands[0], memory, functionName, fileName) - GetOperandValue(Operands[1], memory, functionName, fileName);

                                case TokenType.MulInstruction:
                                    return GetOperandValue(Operands[0], memory, functionName, fileName) * GetOperandValue(Operands[1], memory, functionName, fileName);

                                case TokenType.DivInstruction:
                                    return GetOperandValue(Operands[0], memory, functionName, fileName) / GetOperandValue(Operands[1], memory, functionName, fileName);

                                case TokenType.RemInstruction:
                                    return GetOperandValue(Operands[0], memory, functionName, fileName) % GetOperandValue(Operands[1], memory, functionName, fileName);

                                case TokenType.DEBUGPRINTINSTRUCTION:
                                    int value = GetOperandValue(Operands[0], memory, functionName, fileName);
                                    Console.WriteLine("[PRINT] Value of " + Operands[0].Lexeme + " = " + value);
                                    return value;
                            }

                            return 0;
                        }

                        int GetOperandValue(Token operand, List<Dictionary<string, int>> memory, string functionName, string fileName)
                        {
                            switch (operand.Type)
                            {
                                case TokenType.IntegerLiteral:
                                    return int.Parse(operand.Lexeme);

                                case TokenType.IdentifierLiteral:
                                    for (int i = memory.Count - 1; i >= 0; --i)
                                    {
                                        if (memory[i].ContainsKey(operand.Lexeme))
                                        {
                                            return memory[i][operand.Lexeme];
                                        }
                                    }
                                    break;
                            }

                            ExecutionError("SSA variable not assigned (In function " + functionName + ")", fileName);

                            return 0;
                        }
                    }

                    public static new Expression Parse(List<Token> tokenizedSource, ref int sourceIndex, string fileName)
                    {
                        Expression expression = null;

                        switch (tokenizedSource[sourceIndex].Type)
                        {
                            case TokenType.IdentifierLiteral:
                                expression = Assignment.Parse(tokenizedSource, ref sourceIndex, fileName);
                                break;

                            case TokenType.CallInstruction:
                            case TokenType.NotInstruction:
                            case TokenType.AndInstruction:
                            case TokenType.OrInstruction:
                            case TokenType.XorInstruction:
                            case TokenType.AddInstruction:
                            case TokenType.SubInstruction:
                            case TokenType.MulInstruction:
                            case TokenType.DivInstruction:
                            case TokenType.RemInstruction:
                            case TokenType.DEBUGPRINTINSTRUCTION:
                                expression = Instruction.Parse(tokenizedSource, ref sourceIndex, fileName);
                                break;

                            default:
                                ParserError("Assignment or instruction expected", fileName);
                                break;
                        }

                        if (tokenizedSource[++sourceIndex].Type != TokenType.SemicolonSeparator)
                        {
                            --sourceIndex;
                        }

                        return expression;
                    }

                    public abstract int ExecuteAndGetValue(List<Dictionary<string, int>> memory, AbstractSyntaxTree ast, string functionName, string fileName);

                    public override void Execute(List<Dictionary<string, int>> memory, AbstractSyntaxTree ast, string functionName, string fileName)
                    {
                        ExecuteAndGetValue(memory, ast, functionName, fileName);
                    }
                }

                public new class Block : LowLevel
                {
                    public List<Expression> Expressions { get; set; } = new List<Expression>();

                    public static new Block Parse(List<Token> tokenizedSource, ref int sourceIndex, string fileName)
                    {
                        Block block = new Block();

                        Token braceToken = tokenizedSource[sourceIndex];
                        if (braceToken.Type != TokenType.OpeningBrace)
                        {
                            ParserError("Opening brace expected", fileName);
                        }

                        for (++sourceIndex; sourceIndex < tokenizedSource.Count; ++sourceIndex)
                        {
                            braceToken = tokenizedSource[sourceIndex];
                            if (braceToken.Type == TokenType.ClosingBrace)
                            {
                                break;
                            }

                            block.Expressions.Add(Expression.Parse(tokenizedSource, ref sourceIndex, fileName));
                        }

                        return block;
                    }

                    public override void Execute(List<Dictionary<string, int>> memory, AbstractSyntaxTree ast, string functionName, string fileName)
                    {
                        memory.Add(new Dictionary<string, int>());

                        foreach (Expression expression in Expressions)
                        {
                            expression.Execute(memory, ast, functionName, fileName);
                        }

                        memory.RemoveAt(memory.Count - 1);
                    }
                }

                public static new LowLevel Parse(List<Token> tokenizedSource, ref int sourceIndex, string fileName)
                {
                    LowLevel lowLevel;

                    Token lowLevelDirective = tokenizedSource[sourceIndex];
                    if (lowLevelDirective.Type != TokenType.LowLevelDirective)
                    {
                        ParserError("Low-level directive expected", fileName);
                    }

                    switch (tokenizedSource[++sourceIndex].Type)
                    {
                        case TokenType.OpeningBrace:
                            lowLevel = Block.Parse(tokenizedSource, ref sourceIndex, fileName);
                            break;

                        default:
                            lowLevel = Expression.Parse(tokenizedSource, ref sourceIndex, fileName);
                            break;
                    }

                    return lowLevel;
                }
            }

            public static Statement Parse(List<Token> tokenizedSource, ref int sourceIndex, string fileName)
            {
                Statement statement = null;

                switch (tokenizedSource[sourceIndex].Type)
                {
                    case TokenType.OpeningBrace:
                        statement = Block.Parse(tokenizedSource, ref sourceIndex, fileName);
                        break;

                    case TokenType.LowLevelDirective:
                        statement = LowLevel.Parse(tokenizedSource, ref sourceIndex, fileName);
                        break;

                    default:
                        ParserError("Block or low-level statement expected", fileName);
                        break;
                }

                return statement;
            }

            public abstract void Execute(List<Dictionary<string, int>> memory, AbstractSyntaxTree ast, string functionName, string fileName);
        }

        public class Function
        {
            public string Name { get; set; }
            public Statement Body { get; set; }

            public static Function Parse(List<Token> tokenizedSource, ref int sourceIndex, string fileName)
            {
                Function function = new Function();

                if (tokenizedSource[sourceIndex].Type != TokenType.FunctionKeyword)
                {
                    ParserError("Function expected", fileName);
                }

                Token nameToken = tokenizedSource[++sourceIndex];
                if (nameToken.Type == TokenType.IdentifierLiteral)
                {
                    function.Name = nameToken.Lexeme;
                }
                else
                {
                    ParserError("Function name expected", fileName);
                }

                ++sourceIndex;
                function.Body = Statement.Parse(tokenizedSource, ref sourceIndex, fileName);

                return function;
            }

            public void Execute(List<Dictionary<string, int>> memory, AbstractSyntaxTree ast, string fileName)
            {
                Body.Execute(memory, ast, Name, fileName);
            }
        }

        public static void ParserError(string errorMessage, string currentFile)
        {
            Console.Error.WriteLine("\n");

            Console.Error.WriteLine("[ERROR] " + currentFile + " - Parsing failed!");
            Console.Error.WriteLine(errorMessage);

            Console.Error.WriteLine("\n");
        }

        public static void ExecutionError(string errorMessage, string currentFile)
        {
            Console.Error.WriteLine("\n");

            Console.Error.WriteLine("[ERROR] " + currentFile + " - Execution failed!");
            Console.Error.WriteLine(errorMessage);

            Console.Error.WriteLine("\n");

            System.Environment.Exit(-1);
        }
    }

    class Token
    {
        public TokenType Type { get; set; }
        public string Lexeme { get; set; }
    }

    enum TokenType
    {
        Unknown,

        // Brackets
        OpeningBrace, ClosingBrace,

        // Separators
        CommaSeparator,
        SemicolonSeparator,

        // Operators
        AssignmentOperator,

        // Literals
        IdentifierLiteral,
        IntegerLiteral,

        // Keywords
        FunctionKeyword,

        // Instructions
        CallInstruction,
        NotInstruction,
        AndInstruction,
        OrInstruction,
        XorInstruction,
        AddInstruction,
        SubInstruction,
        MulInstruction,
        DivInstruction,
        RemInstruction,
        DEBUGPRINTINSTRUCTION,

        // Directives
        LowLevelDirective,
    }
}
